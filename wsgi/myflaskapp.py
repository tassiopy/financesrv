#-*- coding: utf-8 -*-

from flask import Flask, request

from db.connection import mongo_db, close_db
from views.auth import auth_blueprint
from views.finance import finance_blueprint

app = Flask(__name__)

app.config['PROPAGATE_EXCEPTIONS'] = True
app.teardown_appcontext(close_db)

#registering blueprints
app.register_blueprint(auth_blueprint, url_prefix="/auth")
app.register_blueprint(finance_blueprint, url_prefix="/finance")

if __name__ == "__main__":
    app.run()

