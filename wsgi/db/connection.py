#-*- coding: utf-8 -*-
import os
import pymongo
from flask import g


def _connect_db():
    g.mongo_conn = pymongo.Connection(os.environ['OPENSHIFT_MONGODB_DB_URL'])
    return g.mongo_conn[os.environ['OPENSHIFT_APP_NAME']]

def mongo_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'mongo_db'):
        g.mongo_db = _connect_db()
    return g.mongo_db

def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'mongo_conn'):
        g.mongo_conn.close()