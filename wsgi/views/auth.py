#-*- coding: utf-8 -*-

from flask import Blueprint, request
from bson import json_util
import json

from error import abort400
from db.connection import mongo_db


auth_blueprint = Blueprint('auth', __name__, template_folder='templates')

@auth_blueprint.route("/login", methods=["PUT", "POST"])
def login():
    user_data = request.get_json(force=True)
    users = mongo_db().users.find({"login": user_data['login'], "password": user_data['password']})
    
    if users.count() > 0: 
        user = users[0]
        user.pop('password')
        user.pop('_id')
        result = {'loggedIn': True, 'user': user}
    else:
        result = {'loggedIn': False}

    return str(json.dumps(result, default=json_util.default))

@auth_blueprint.route("/signup", methods=["PUT", "POST"])
def createUser():
    user_data = request.get_json(force=True)
    if not all([user_data.has_key(key) for key in ('login', 'password', 'email')]):
        return abort400('Os atributos login, password e email devem ser especificados.')

    if mongo_db().users.find({'email': user_data['email']}).count() > 0:
        return abort400('Esse email já está sendo utilizado.')

    created = mongo_db().users.insert(user_data)

    return str(json.dumps({'created': created},default=json_util.default))