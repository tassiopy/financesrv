#-*- coding: utf-8 -*-

from flask import Blueprint, request
from bson import json_util
import json

from error import abort400

from db.connection import mongo_db

finance_blueprint = Blueprint('finance', __name__, template_folder='templates')


def removeIds(objects):
    result = []
    for obj in objects:
        obj.pop('_id')
        result.append(obj)
    return result

@finance_blueprint.route("/categorias/<int:userId>")
def findCategoriaByUser(userId):
    result = mongo_db().categorias.find({'idUsuario': userId})

    return str(json.dumps({'results' : removeIds(result)},default=json_util.default))

@finance_blueprint.route("/categorias", methods=["PUT", "POST"])
def createCategory():
    category_data = request.get_json(force=True)
    if not category_data.has_key('idUsuario'):
        return abort400('O atributo idUsuario deve ser especificado.')

    created = mongo_db().categorias.insert(category_data)

    return str(json.dumps({'created': created},default=json_util.default))



@finance_blueprint.route("/carteiras/<int:userId>")
def findCarteiraByUser(userId):
    result = mongo_db().carteiras.find({'idUsuario': userId})
    
    return str(json.dumps({'results' : removeIds(result)},default=json_util.default))

@finance_blueprint.route("/carteiras", methods=["PUT", "POST"])
def createCarteira():
    carteira_data = request.get_json(force=True)
    if not carteira_data.has_key('idUsuario'):
        return abort400('O atributo idUsuario deve ser especificado.')

    created = mongo_db().carteira.insert(carteira_data)

    return str(json.dumps({'created': created},default=json_util.default))



@finance_blueprint.route("/lancamentos/<int:userId>")
def findLancamentosByUser(userId):
    result = mongo_db().lancamentos.find({'idUsuario': userId})
    
    return str(json.dumps({'results' : removeIds(result)},default=json_util.default))

@finance_blueprint.route("/lancamentos", methods=["PUT", "POST"])
def createLancamento():
    lancamento_data = request.get_json(force=True)
    if not lancamento_data.has_key('idUsuario'):
        return abort400('O atributo idUsuario deve ser especificado.')

    created = mongo_db().lancamentos.insert(lancamento_data)

    return str(json.dumps({'created': created},default=json_util.default))