from flask import jsonify

def abort400(message):
    response = jsonify({'code': 400,'message': message})
    response.status_code = 400
    return response